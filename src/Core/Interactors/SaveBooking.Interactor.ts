import Passenger from "../Entities/Passenger";
import Trip from "../Entities/Trip";
import TripRepository from "../Repositories/Trip.Repository";
import Ticket from '../Entities/Ticket';
import NotifierRepository from "../Repositories/Notifier.Repository";

// Send repository throught DI
const SaveBooking = (tripRepository: TripRepository, notifierRepository: NotifierRepository
) => async (passenger: Passenger, tripId: string): Promise<Ticket> => {
    
    // Get trip by Id
    const trip: Trip = await tripRepository.getById(tripId);

    // Crear ticket
    const ticket: Ticket = {
        tripId: trip.id,
        tripName: trip.name,
        departureTime: trip.departureTime,
        arrivalTime: trip.arrivalTime,
        passengerName: passenger.name,
        passengerSurname: passenger.surname
    };
    
    // Notificar pasajero
    notifierRepository.notify(ticket, passenger.email);
    
    // Devuelve ticket
    return ticket;
}

export default SaveBooking;