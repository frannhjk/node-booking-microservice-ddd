import EmailNotifier from "../../DataSources/EmailNotifier.Datasource";
import TripMongo from "../../DataSources/TripMongo.Datasource";
import SaveBooking from "./SaveBooking.Interactor";

const tripRepository = new TripMongo();
const notifierRepository = new EmailNotifier();

export default SaveBooking(tripRepository, notifierRepository);