import Ticket from "../Entities/Ticket";

export default interface NotifierRepository {
    notify(ticket: Ticket, email: string) : void;
}