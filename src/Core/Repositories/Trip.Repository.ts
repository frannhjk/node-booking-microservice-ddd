import Trip from "../Entities/Trip";

export default interface TripRepository{
    getById(id: string): Promise<Trip>;
}