import Ticket from "../Core/Entities/Ticket";
import NotifierRepository from "../Core/Repositories/Notifier.Repository";
import nodemailer from 'nodemailer';

export default class EmailNotifier implements NotifierRepository {


    public async notify(ticket: Ticket, email: string): Promise<void> {
        
        const mailOptions = {
            from: "Nodeando con Franco",
            to: email,
            subject: `Tu ticket para ${ticket.tripName}`,
            text: "Te llego el ticket crack!!"        
        };
        
        this.getMailer().sendMail(mailOptions);
    }


    private getMailer() {
        return nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true,
            auth: {
                user: "testuser@gmail.com",
                pass: "123124"
            }
        });
    }
}