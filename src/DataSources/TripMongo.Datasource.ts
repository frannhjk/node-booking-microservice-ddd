import Trip from "../Core/Entities/Trip";
import TripRepository from "../Core/Repositories/Trip.Repository";
import * as MongoClient from 'mongodb';

// Class to use implement from TS
export default class TripMongo implements TripRepository {
    
    public async getById(id: string): Promise<Trip> {
        const collection = await this.getCollection(); // TODO: Refactor
        const trip: any = await collection.findOne({ id });

        return trip;
    }


    private async getCollection() {
        const url = "mongodb://localhost:27017";
        const client = await MongoClient.MongoClient.connect(url);

        const db = client.db("ticketing");
        return db.collection("trips");
    }
}