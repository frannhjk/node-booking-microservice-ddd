import { Request, Response } from 'express';
import saveBooking from '../Core/Interactors';

const bookingController = async (request: Request, response: Response) => {
    
    // Obtenemos el body del request
    const { body } = request; 

    // Obtenemos el pasajero el trip del body
    const { passenger, trip } = body;

    const ticket = await saveBooking(passenger, trip);
    
    response.json(ticket);
}

export default bookingController;